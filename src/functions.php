<?php

require_once __DIR__."/MysqlReadWrite.php";

function mysqli_connect_rw($host = '', $user = '', $password = '', $database = '', $port = '', $socket = ''){
    return \MysqlReadWrite\MysqlReadWrite::getInstance()->mysqli_connect($host, $user, $password, $database, $port, $socket);
}

function mysql_connect_rw($host = '', $user = '', $password = '', $database = '', $port = '', $socket = ''){
    return \MysqlReadWrite\MysqlReadWrite::getInstance()->mysqli_connect($host, $user, $password, $database, $port, $socket);
}

function mysqli_query_rw($link, $query, $resultmode = MYSQLI_STORE_RESULT){
    return \MysqlReadWrite\MysqlReadWrite::getInstance()->mysqli_query($link, $query, $resultmode);
}

function mysql_query_rw($query, $link = null){
    return \MysqlReadWrite\MysqlReadWrite::getInstance()->mysqli_query($link, $query);
}

function mysqli_set_charset_rw($link, $charset){
    return \MysqlReadWrite\MysqlReadWrite::getInstance()->mysqli_set_charset($charset, $link);
}

function mysql_set_charset_rw($charset){
    return \MysqlReadWrite\MysqlReadWrite::getInstance()->mysqli_set_charset($charset);
}

function mysqli_select_db_rw($database){
    return \MysqlReadWrite\MysqlReadWrite::getInstance()->mysqli_select_db($database);
}

function mysql_select_db_rw($database, $link = null){
    return \MysqlReadWrite\MysqlReadWrite::getInstance()->mysqli_select_db($database, $link);
}

function mysqli_error_rw(){
    return \MysqlReadWrite\MysqlReadWrite::getInstance()->mysqli_error();
}

function mysql_error_rw($link = null){
    return \MysqlReadWrite\MysqlReadWrite::getInstance()->mysqli_error($link);
}
