<?php

namespace MysqlReadWrite;

class MysqlReadWrite{

    const MODE_READ = "read";
    const MODE_WRITE = "write";

    protected static $instance;
    protected $fileLog;

    protected $connections = [
        "share" => null,
        "read" => null,
        "write" => null,
    ];

    protected $lastQueryConnection = null;

    public static function getInstance(){

        if(is_null(self::$instance)){
            self::$instance = new MysqlReadWrite();
        }

        return self::$instance;
    }

    public function setLogFile($file){
        $this->fileLog = $file;
    }

    public function log($content){

        if(!is_null($this->fileLog)){
            file_put_contents($this->fileLog, $content."\n", FILE_APPEND);
        }
    }

    public function connect($mode, $host = '', $user = '', $password = '', $database = '', $port = '', $socket = ''){

        if(!in_array($mode, [self::MODE_READ, self::MODE_WRITE])){
            return false;
        }

        $result = mysqli_connect($host, $user, $password, $database, $port, $socket);

        if($result !== false){
            $this->connections[$mode] = $result;
        }

        return $result;
    }

    public function checkForAllConnections($query, $link = null){

        $query = trim($query);

        $forAll = (strpos(strtolower($query), "set") === 0);

        if($forAll){

            foreach ($this->connections as $mode => $connection) {

                if(!is_null($connection)){

                    $this->lastQueryConnection = $connection;

                    $this->log("set query: {$query}");

                    mysqli_query($connection, $query);
                }
            }

            if(!is_null($link)){

                $this->lastQueryConnection = $link;

                $this->log("set query: {$query}");

                mysqli_query($link, $query);
            }
        }

        return $forAll;
    }

    public function getLinkByQueryAndDefaultLink($query, $link = null){

        $query = trim($query);

        $mode = self::MODE_WRITE;

        if(strpos(strtolower($query), "select") === 0){
            $mode = self::MODE_READ;
        }

        if(strpos(strtolower($query), "show") === 0){
            $mode = self::MODE_READ;
        }

        if($mode == self::MODE_READ){

            if(!is_null($this->connections[self::MODE_READ])){
                $link = $this->connections[self::MODE_READ];
            }

        }else{

            if(!is_null($this->connections[self::MODE_WRITE])){
                $link = $this->connections[self::MODE_WRITE];
            }
        }

        if(is_null($link)){
            $link = $this->connections['share'];
        }

        return $link;
    }

    public function mysqli_connect($host = '', $user = '', $password = '', $database = '', $port = '', $socket = ''){

        $result = mysqli_connect($host, $user, $password, $database, $port, $socket);

        if($result !== false){
            $this->connections['share'] = $result;
        }

        return $result;
    }

    public function mysqli_query($link, $query, $resultmode = MYSQLI_STORE_RESULT){

        $this->log("query: {$query}");

        if($this->checkForAllConnections($query, $link)){
            return true;
        }

        $link = $this->getLinkByQueryAndDefaultLink($query, $link);

        $this->lastQueryConnection = $link;

        return mysqli_query($link, $query, $resultmode);
    }

    public function mysqli_set_charset($charset, $link = null){

        $this->log("charset set {$charset}");

        $result = true;

        foreach ($this->connections as $mode => $connection) {

            if(!is_null($connection)){

                $this->lastQueryConnection = $connection;

                if(!mysqli_set_charset($connection, $charset)){
                    $result = false;
                }
            }
        }

        if(!is_null($link)){

            $this->lastQueryConnection = $link;

            if(!mysqli_set_charset($link, $charset)){
                $result = false;
            }
        }

        return $result;
    }

    public function mysqli_select_db($database_name, $link = null) {

        $result = true;

        foreach ($this->connections as $mode => $connection) {

            if(!is_null($connection)){

                $this->lastQueryConnection = $connection;

                if(mysqli_select_db($connection, $database_name)){
                    $result = false;
                }
            }
        }

        if(!is_null($link)){

            $this->lastQueryConnection = $link;

            if(mysqli_select_db($link, $database_name)){
                $result = false;
            }
        }

        return $result;
    }

    public function mysqli_error($link = null){

        if(is_null($link)){
            $link = $this->lastQueryConnection;
        }

        return mysqli_error($link);
    }
}
